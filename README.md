# Using Linear Programming to determine the role of plant- and animal-sourced foods in least-cost nutritionally adequate diets for adults

Supplementary material associated with the paper under submission to Current Developments in Nutrition - Using Linear Programming to determine the role of plant- and animal-sourced foods in least-cost nutritionally adequate diets for adults - SMS Chungchunlam, DP Garrick, PJ Moughan

## Dependencies

## Install Julia 1.6.1
If you do not have Julia already installed, follow the installation instructions to download Julia 1.6.1 [here](https://julialang.org/downloads/oldreleases/).

In MacOS and Linux, using [juliaup](https://github.com/JuliaLang/juliaup) allows you to install and manage multiple Julia versions.

## Installing necessary packages and running an analysis

Instructions for MacOS or Linux users:

The repository can be cloned and `julia` started on the command line (i.e. in the Terminal app) using the following commands:
```
git clone https://gitlab.com/dpgarrick/cdn-s-21-00186.git
cd cdn-s-21-00186
julia
```
If `git` is run for the first time, you may be prompted to install the Xcode Command Line Tools. If you are not familiar with the command line, Julia can be installed directly from the Julia website.

Please proceed from Step 3 of the General Instructions below, making note of the directory path where you cloned the repository.


General Instructions:
1. Download (or `git clone`) the repository. A .zip file of the current repository can be downloaded [here](https://gitlab.com/dpgarrick/cdn-s-21-00186/-/archive/main/cdn-s-21-00186-main.zip). Unzip the repository and record the full directory path.
2. Start the `julia` app.
3. In the julia REPL, type in `cd("C:/example/path/to/your/repository/cdn-s-21-00186")` but with the directory path you noted previously in place of `C:/example/path/to/your/repository/cdn-s-21-00186`. Press Enter.
**3a.** Type `using Pkg; Pkg.activate("."); Pkg.instantiate()` and Press Enter. This should download and install all of the necessary packages. The first time may take a while to download and install all the necessary packages, but should be much faster thereafter. 
**3b.** Type `using Pluto; Pluto.run()` and Press Enter. This should open the Pluto notebook interface. 
3. In the Pluto notebook interface, in the path bar below "Open from file:", type the full path to the notebook.jl file in the repository, e.g. `C:/example/path/to/your/repository/cdn-s-21-00186/notebook.jl` and press "Open".
4. The notebook should immediately run all cells, assuming that the data directory is the current working directory. Upon the first time opening the notebook, additional time may be needed for `julia` to pre-compile all the packages.
5. If the notebook does not run, the most likely error is that the working directory needs to be changed to point to the location of the associated files downloaded with the repository (NZFoods.csv and NZPersonReq.csv files). In that case, the `dataDir` variable in the Pluto notebook will need to be updated with the path to the directory of the git repository that was downloaded (i.e. where the notebook.jl file exists).
6. The notebook contains instructions and information about the various settings that can be dynamically changed to observe the change in the dietary composition and minimized cost of the modelled diets. The constraints that can be changed dynamically include the daily energy and nutrient requirements for an adult human and the price of various food gorups.  


### Error

If the following error is seen in the Julia REPL while running Pluto (typically accompanied by an error about compiling StatsPlots in the Pluto notebook):
```
From worker 2:	Your GR installation is incomplete. Rerun build step for GR package.
From worker 2:	ERROR: LoadError: LoadError: InitError: Evaluation into the closed module `GR` breaks incremental compilation because the side effects will not be permanent. This is likely due to some other module mutating `GR` with `eval` during precompilation - don't do this.
```
Exit the Pluto notebook and return to the `julia` REPL. The command `]build Plots` should be typed (do not copy and paste), to rebuild the GR module and fix the error. Once completed, Julia should be restarted and the General Instructions followed from Step 3b to run the Pluto notebook.   



## Outputs

For each analysis an associated directory will be created in the "Results" folder where the source file notebook.jl exists. The following results files will be created for the cost-minimized optimized diet solutions; the baseline modelled diet and any scaled diets that were affected by changes in the dynamic constraints, such as food prices modified using cost scale factors.

* _optimDietFoods.csv contains information about the "Foods" in the least-cost optimized diet solution
* _optimDietNutrientSummary.csv contains information about the "Nutrient" requirements for an adult human and the amount of each nutrient provided by the least-cost optimized diet solution
* _optimDietSubGroupSummary.csv contains information about the total servings and cost from each food "Subgroup" included in the least-cost modelled diet solution 
* _optimDietGroupSummary.csv contains information about the total servings from each food "Group" included in the least-cost modelled diet solution
* _minNutShadowPrice.csv contains information about the shadow prices that are available for the nutrient constraints met at "Minimum"
* _maxNutShadowPrice.csv contains information about the shadow prices that are available for the nutrient constraints met at "Maximum"
* _foodAmtShadowPrice.csv contains information about the shadow prices that are available for the "Amount" constraint for each food item that was met at the maximum allowable intake.  


The following plots are also generated

* _SubGroupCostComparison.png - compares the "Cost" from each food subgroup included in the baseline and scaled least-cost optimized diet solutions 
* _SubGroupComparison.png - compares the "Servings" from each food "Subgroup" included in the baseline and scaled least-cost optimized diet solutions 
* _GroupComparison.png - compares the "Servings" from each food "Group" included in the baseline and scaled least-cost optimized diet solutions 


# Software versions
The current study used version 1.6.1 for Julia, version 0.21.8 for JuMP, and version 0.14.8 for Pluto - All the information regarding the versions used in the analysis are detailed in the Manifest.toml file.

### Updating packages 

The packages are pinned to a particular version via the Pkg.activate() and Pkg.instantiate() commands. 
However, to update all Julia packages, in the `julia` REPL, type `import Pkg; Pkg.update()` and Press Enter. 
Or, to update a particular package such as Pluto, in the `julia` REPL, type `import Pkg; Pkg.update("Pluto")` and Press Enter. 


# Acknowledgments

We gratefully acknowledge the creators of the Julia language and associated packages including JuMP and Pluto in particular.
~

